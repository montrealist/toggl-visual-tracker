# Toggl visual tracker

Simple UI that shows entries from Toggl as a chart. Tags correspond to time entries - can click to include/exclude from the chart.

Deployed here: https://toggl-visual-tracker.now.sh/

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Simple front end for an API endpoint that connects to Toggl and fetches your time entries, then displays it to you as a pie chart.

The idea arose from wanting to play with React and at the same time produce something useful for myself.

Was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Demo

http://toggl-visual-tracker.montrealist.now.sh/

## Local development

In the project directory, you can run:

### `npm i && npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

import React, { useState, useRef, useEffect, useCallback } from "react";

import axios from "axios";

import "bulma/css/bulma.min.css";
import "./App.css";

import TagPicker from "components/TagPicker.js";
import DateSelector from "components/DateSelector.js";
import Notification from "components/Notification.js";
import Loading from "components/Loading.js";
import Chart from "components/Chart.js";

import {
    convertDateStringToApiFormat,
    gatherTagsFromEvents,
    transformEntriesForNewChart,
} from "helpers/helpers";

function App() {
    const [{ from, to, tags, entries }, setState] = useState({
        from: null,
        to: null,
        tags: [],
        entries: {},
    });

    const [dataLoaded, setDataLoaded] = useState(null);
    const [statusMessage, setStatusMessage] = useState(null);

    const previousDates = useRef({ from, to });
    const previousEntries = useRef(entries);

    useEffect(() => {
        const fetchData = async () => {
            setDataLoaded(false);

            // give user time to figure out what is happening + prevent 429 error coming back from API:
            setTimeout(() => {
                const instance = axios.create({
                    baseURL: process.env.REACT_APP_API_URL,
                });

                const params = {
                    since: from ? convertDateStringToApiFormat(from) : "",
                    until: to ? convertDateStringToApiFormat(to) : "",
                };

                instance
                    .get(process.env.REACT_APP_API_ENDPOINT, {
                        params,
                    })
                    .then((response) => {
                        const events = response.data.data;
                        const entries = transformEntriesForNewChart(events);
                        const tags = gatherTagsFromEvents(events);

                        setState((prev) => ({
                            ...prev,
                            tags,
                            entries,
                        }));

                        previousEntries.current = { ...entries };
                        setDataLoaded(true);

                    })
                    .catch((e) => {
                        console.error("error after axios.get():", e);
                        setStatusMessage(e.message);
                    });
            }, 1500);
        };

        // FIXME: 1. this compares exact dates and times with seconds - will always be different!
        // console.log('previous', previousDates.current.from, previousDates.current.to);
        // console.log('now', from, to);
        // console.log('comparisons', previousDates.current.from !== from, previousDates.current.to !== to);

        // FIXME: 2. read this answer and use UTC dates: https://stackoverflow.com/a/38050824/65232
        if (
            previousDates.current.from !== from &&
            previousDates.current.to !== to
        ) {
            fetchData();
            previousDates.current = { from, to };
        }
    }, [from, to]);

    const onDatesUpdate = useCallback((from, to) => {
        console.log('onDatesUpdate ran in parent', from, to);
        setState(prev => ({
            ...prev,
            from,
            to
        }))
    }, []);

    const onToggleTag = (id, label) => {
        const updatedTags = [...tags];
        const tag = updatedTags[id];
        let updatedEntries;
        if (tag.disabled) {
            // if tag was disabled means we're enabling it so need to add the corresponding entry back to entries array:
            const entry = previousEntries.current[tag.name];
            updatedEntries = Object.assign({}, entries);
            updatedEntries[tag.name] = entry;
        } else {
            // remove the corresponding entry from entries array:
            // TODO: can we compare entry's position in array with id here?
            updatedEntries = Object.keys(entries).reduce((obj, key) => {
                if (key !== label) {
                    obj[key] = entries[key];
                }
                return obj;
            }, {});

            // filter((entry) => entry.title !== label);
        }

        updatedTags[id] = {
            ...tag,
            disabled: !tag.disabled,
        };

        setState((prev) => ({
            ...prev,
            tags: updatedTags,
            entries: updatedEntries,
        }));

        // if (updatedEntries.length) {

        // } else {
        //     const message = "Let's not remove the last tag, shall we.";
        // TODO: show warning to user
        //     setStatusMessage(message);
        // }
    };

    return (
        <div className="App">
            <section className="hero is-fullheight">
                {/* TODO: extract nav bar into its own React component? */}
                <div className="hero-head">
                    <nav className="navbar">
                        <div className="container">
                            <div className="navbar-brand">
                                <h1 className="navbar-item">
                                    Toggl Visual Tracker
                                </h1>
                            </div>
                        </div>
                    </nav>
                </div>

                <div className="hero-body" style={{ alignItems: 'start' }}>
                    <div className="container">
                        <div className="columns is-centered">
                            <div className="column is-half">
                                <Notification
                                    className="status block"
                                    visible={statusMessage ? true : false}
                                    message={statusMessage}
                                    onDismiss={() => setStatusMessage(null)}
                                ></Notification>
                                <DateSelector
                                    active={dataLoaded}
                                    onUpdate={onDatesUpdate}
                                ></DateSelector>
                                {!dataLoaded ? (<Loading />) : (
                                    <>
                                        <TagPicker
                                            tags={tags}
                                            onToggle={onToggleTag}
                                        ></TagPicker>
                                        <div className="chart">
                                            <Chart data={entries}
                                            />
                                            {/* <PieChart
                                        style={{
                                            height: "50vh",
                                            margin: "2em",
                                        }}
                                        viewBoxSize={[450, 450]}
                                        label={({ data, dataIndex }) => {
                                            return data[dataIndex].title;
                                        }}
                                        labelPosition={60}
                                        labelStyle={{
                                            fill: "#fff",
                                            fontFamily: "sans-serif",
                                            fontSize: "17px",
                                            fontWeight: "bold",
                                        }}
                                        lengthAngle={360}
                                        lineWidth={100}
                                        paddingAngle={0}
                                        radius={50}
                                        data={entries}
                                    /> */}
                                        </div>
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default App;

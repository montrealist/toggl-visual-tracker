import React from "react";
// import ReactDOM from 'react-dom';
import { VictoryAxis, VictoryBar, VictoryChart, VictoryLabel, VictoryStack } from 'victory';

export default function Chart(props) {

    const labels = {
        '60days-interview-prep': 'Interview prep',
        '60days-coding': 'Coding',
        '60days-applying': 'Applying to jobs',
        '60days-other': 'Other (leetcode, etc.)'
    };

    const plannedPercentages = {
        '60days-interview-prep': 30,
        '60days-coding': 40,
        '60days-applying': 15,
        '60days-other': 15
    };

    const relevantTags = Object.keys(labels);
    
    const filtered = Object.keys(props.data)
    .filter(key => relevantTags.includes(key))
    .reduce((obj, key) => {
        return {
            ...obj,
            [key]: props.data[key]
          };
    }, {});

    const dataOnLeft = Object.keys(plannedPercentages).map((key) => {
        return {
            x: labels[key],
            y: plannedPercentages[key]
        };
    });

    const dataOnRight = Object.keys(plannedPercentages).map((key) => {
        if(!filtered[key]) {
            return {
                x: labels[key],
                y: 0
            };
        }
        return {
            x: labels[key],
            y: filtered[key]
        };
    });

    const width = 400;

    return (
        <VictoryChart horizontal
            height={300}
            width={width}
            padding={40}
        >
            <VictoryStack
                style={{ data: { width: 25 }, labels: { fontSize: 15 } }}
            >
                <VictoryBar
                    style={{ data: { fill: "tomato" } }}
                    data={dataOnLeft}
                    y={(data) => (-Math.abs(data.y))}
                    labels={({ datum }) => (`${Math.abs(datum.y)}%`)}
                />
                <VictoryBar
                    style={{ data: { fill: "orange" } }}
                    data={dataOnRight}
                    labels={({ datum }) => (`${Math.abs(datum.y)}%`)}
                />
            </VictoryStack>

            <VictoryAxis
                style={{
                    axis: { stroke: "transparent" },
                    ticks: { stroke: "transparent" },
                    tickLabels: { fontSize: 15, fill: "black" }
                }}
                /*
                  Custom tickLabelComponent with
                  an absolutely positioned x value to position
                  tick labels in the center of the chart and custom y value to 
                  raise up above the band
                */
                tickLabelComponent={
                    <VictoryLabel
                        x={width / 2}
                        dy={-30}
                        height={200}
                        textAnchor="middle"
                    />
                }
                tickValues={dataOnLeft.map((point) => point.x).reverse()}
            />
        </VictoryChart>
        // TODO: maybe have this?
        // {!props.data && (
        //     <div className="notification is-warning is-light">
        //         Please pass some data to the Chart component.{" "}
        //         <span role="img" aria-label="high five">
        //             🙏🏼
        //         </span>
        //     </div>
        // )}
    );
}

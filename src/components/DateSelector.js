import React, { useState, useEffect } from "react";
import { getPreviousMonday, getPreviousSunday } from "helpers/helpers";

export default function DateSelector({ from, to, onUpdate, active }) {
    const LAST_7 = "LAST_7";
    const LAST_WEEK = "LAST_WEEK";
    const LAST_30 = "LAST_30";

    const getDatesFromFilterValue = (filter) => {
        let from = new Date();
        let to = new Date();
        switch (filter) {
            case LAST_7:
                from.setDate(from.getDate() - 7);
                break;
            case LAST_WEEK:
                from = getPreviousMonday(from);
                to = getPreviousSunday(to);
                break;
            case LAST_30:
                from.setDate(from.getDate() - 30);
                break;
            default:
                break;
        }
        return { from, to };
    };

    let dateFrom, dateTo, filter;
    if (!from && !to) {
        filter = LAST_30; // show default filter in UI
        ({ to: dateTo, from: dateFrom } = getDatesFromFilterValue(filter));
    } else if (to && from) {
        filter = null;
        dateFrom = new Date(from);
        dateTo = new Date(to);
    } else {
        filter = null;
        if (to && !from) {
            dateTo = new Date(to);
            dateFrom = new Date();
            dateFrom.setDate(dateTo.getDate() - 7);
        }
        if (from && !to) {
            dateFrom = new Date(from);
            dateTo = new Date();
            dateTo.setDate(dateFrom.getDate() + 7);
        }
    }

    const [state, setState] = useState({
        from: dateFrom,
        to: dateTo,
        filter,
    });

    useEffect(() => {
        onUpdate
            ? onUpdate(state.from, state.to)
            : console.warn("onUpdate is not in props!");
    }, [state.from, state.to, onUpdate]);

    const handleClick = (e) => {
        const filter = e.target.value;
        const { from, to } = getDatesFromFilterValue(filter);
        setState((prev) => ({ ...prev, from, to, filter }));
    };

    const dateFilters = [
        { id: 1, filter: LAST_7, label: "Last 7 days" },
        { id: 2, filter: LAST_WEEK, label: "Last week" },
        { id: 3, filter: LAST_30, label: "Last 30 days" },
    ];

    return (
        <section className="date-seletor block">
            <div className="content">
                <input
                    className="input"
                    type="text"
                    readOnly={true} // TODO: eventually remove readOnly when adding functionality for entering custom dates
                    placeholder="Text input"
                    value={`${state.from.toDateString()} to ${state.to.toDateString()}`}
                ></input>
            </div>

            <div className="buttons">
                {dateFilters.map((control) => (
                    <button
                        disabled={active === false}
                        key={control.id}
                        className={`button ${
                            state.filter === control.filter ? "is-info" : "nope"
                        }`}
                        onClick={handleClick}
                        value={control.filter}
                    >
                        {control.label}
                    </button>
                ))}
            </div>
        </section>
    );
}

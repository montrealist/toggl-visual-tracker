import React from "react";
import { motion } from 'framer-motion';

export default function Loading(props) {

  return (
    <div className="wrapper-item-loading">
      <motion.div
        className="item-loading"
        animate={{
          rotate: [0, 0, 270, 270, 0],
          borderRadius: ["20%", "20%", "50%", "50%", "20%"],
        }}
        transition={{
          duration: 2,
          ease: "easeInOut",
          times: [0, 0.2, 0.5, 0.8, 1],
          loop: Infinity,
          repeatDelay: 1
        }}
      />
      <p className="msg">{!props.message ? 'Loading...' : props.message}</p>
    </div>
  );
}

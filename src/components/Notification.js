import React from "react";

export default function Notification(props) {
    return (
        <div
            className={
                "notification is-warning is-light " +
                (props.visible ? "" : "hidden")
            }
        >
            <button className="delete" onClick={props.onDismiss}>
                <span>Dismiss message</span>
            </button>
            Oops! {props.message}. Try refreshing the page.
        </div>
    );
}

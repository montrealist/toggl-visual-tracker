import React from "react";

export default function Tag(props) {
    return (
        <li
            key={props.id ? props.id.toString() : "INVALID"}
            className={"control " + (props.disabled ? "disabled" : "")}
        >
            <span className="tags has-addons">
                <span
                    className={
                        "tag " + (props.disabled ? "is-light" : "is-info")
                    }
                >
                    {props.name}
                </span>
                <button
                    className="tag is-delete"
                    onClick={() =>
                        "onToggle" in props
                            ? props.onToggle(props.id, props.name)
                            : console.warn("props.onToggle is not defined!")
                    }
                >
                    <span className="hidden-screen-reader">
                        Toggle this tag
                    </span>
                </button>
            </span>
        </li>
    );
}

import React, { Fragment } from "react";
import Tag from "components/Tag";

export default function TagPicker(props) {
    const tags = props.tags;

    return (
        <section className="tag-picker block">
            {tags && (
                <Fragment>
                    <ul className="field is-grouped is-grouped-multiline">
                        {tags.map((tag) => {
                            return (
                                <Tag
                                    key={tag.id}
                                    id={tag.id.toString()}
                                    name={tag.name}
                                    onToggle={props.onToggle}
                                    disabled={tag.disabled}
                                ></Tag>
                            );
                        })}
                    </ul>
                </Fragment>
            )}
            {!tags && (
                <div className="box">
                    <p>Boo, I didn't receive tags to display.</p>
                </div>
            )}
        </section>
    );
}

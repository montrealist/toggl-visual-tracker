import React from "react";

import { render, cleanup } from "@testing-library/react";

import DateSelector from "components/DateSelector";

afterEach(cleanup);

it("renders without crashing", () => {
    render(<DateSelector />);
});

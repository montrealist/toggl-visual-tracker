import React from "react";

import { render, cleanup } from "@testing-library/react";

import Notification from "components/Notification";

afterEach(cleanup);

it("renders without crashing", () => {
    render(<Notification />);
});

import React from "react";

import { render, cleanup } from "@testing-library/react";

import Tag from "components/Tag";

afterEach(cleanup);

it("renders without crashing", () => {
    render(<Tag />);
});

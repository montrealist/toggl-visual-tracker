import React from "react";

import { render, cleanup } from "@testing-library/react";

import TagPicker from "components/TagPicker";

afterEach(cleanup);

it("renders without crashing", () => {
    render(<TagPicker />);
});

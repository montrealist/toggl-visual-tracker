// from here: https://stackoverflow.com/a/46544455/65232
export function getPreviousMonday(date = null) {
    let prevMonday = (date && new Date(date.valueOf())) || new Date();
    // console.log(
    //     prevMonday.getDate(),
    //     prevMonday.getDay(),
    //     (prevMonday.getDay() + 6) % 7
    // );
    prevMonday.setDate(
        prevMonday.getDate() - ((prevMonday.getDay() + 6) % 7) - 7
    );
    return prevMonday;
}

// from here: https://stackoverflow.com/a/46544455/65232
export function getPreviousSunday(date = null) {
    let prevSunday = (date && new Date(date.valueOf())) || new Date();
    // console.log(
    //     prevSunday.getDate(),
    //     prevSunday.getDay(),
    //     ((prevSunday.getDay() + 6) % 7) - 1
    // );
    prevSunday.setDate(
        prevSunday.getDate() - ((prevSunday.getDay() + 6) % 7) - 1
    );
    return prevSunday;
}

export function convertDateStringToApiFormat(dateString) {
    const d = new Date(dateString);
    const dtf = new Intl.DateTimeFormat("en", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
    });
    const [{ value: mo }, , { value: da }, , { value: ye }] = dtf.formatToParts(
        d
    );

    return `${ye}-${mo}-${da}`;
}

// const colourSets = [
//     [
//         "#AEB8BA",
//         "#454545",
//         "#88989C",
//         "#687b79",
//         "#61777E",
//         "#3B5760",
//         "#143642",
//         "#374140",
//         "#546E7A",
//         "#616161",
//         "#37474F",
//         "#2A2C2B",
//         "#31353D",
//         "#26667D",
//     ],
//     [
//         "#512DA8",
//         "#673AB7",
//         "#9575CD",
//         "#420F8D",
//         "#732DD9",
//         "#553285",
//         "#36175E",
//         "#9768D1",
//         "#4A148C",
//         "#7B52AB",
//         "#7E55A3",
//         "#6B14A6",
//         "#360259",
//         "#4C1273",
//         "#5C148C",
//         "#460273",
//         "#3C0F59",
//         "#7B1FA2",
//     ],
//     [
//         "#0092B2",
//         "#00ABD8",
//         "#04668C",
//         "#0288D1",
//         "#0067A6",
//         "#012840",
//         "#2980B9",
//         "#3498DB",
//         "#00305A",
//         "#004B8D",
//         "#1976D2",
//         "#133463",
//         "#002253",
//         "#1B76FF",
//         "#0D47A1",
//         "#365FB7",
//         "#2962FF",
//         "#35478C",
//         "#1C3FFD",
//         "#020873",
//         "#0003C7",
//         "#1510F0",
//         "#2C1DFF",
//     ],
// ];
// const colours = colourSets[Math.floor(Math.random() * colourSets.length)];

export function getAllTags(apiObjects) {
    const tagArrays = apiObjects.map(event => event.tags);
    const tags = tagArrays.flat();
    return [...new Set(tags)];
}

export function gatherTagsFromEvents(events) {
    const tags = getAllTags(events);
    return tags.map((val, index) => ({
        id: index,
        name: val,
        disabled: false,
    }));
}

export function transformEntriesForNewChart(entries) {
    const tagsAndTotals = transformEntriesForChart(entries);

    // get total time and calculate percentages from that
    const totalTime = Object.values(tagsAndTotals).reduce((accumulator, current) => accumulator + current);

    Object.keys(tagsAndTotals).forEach((key, index) => { // index: the ordinal position of the key within the object 
        tagsAndTotals[key] = Math.round( (tagsAndTotals[key]/totalTime) * 100 );
    });
    return tagsAndTotals;
}

export function transformEntriesForChart(entries) {

    const tagsAndTotals = {};
    const bareEntries = entries.map((entry) => ({
        duration: entry.dur,
        tags: entry.tags,
    }));

    // console.log('bareEntries', bareEntries.length);

    // sum the durations for each tag:
    for (const entry of bareEntries) {
        for (const tag of entry.tags) {
            if (!(tag in tagsAndTotals)) {
                tagsAndTotals[tag] = 0;
            }
            tagsAndTotals[tag] += entry.duration;
        }
    }

    // console.log('tags', tagsAndTotals);
    return tagsAndTotals;
}

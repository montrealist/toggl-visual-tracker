import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import TagPicker from "components/TagPicker.js";
import Tag from "components/Tag.js";
import DateSelector from "components/DateSelector.js";
import Chart from "components/Chart.js";
import "bulma/css/bulma.min.css";
import "index.css";

const dummyTags = [
    { id: 1, name: "work" },
    { id: 2, name: "leisure" },
    { id: 3, name: "some super long named tag for whatever reason" },
    { id: 4, name: "another super long named tag for whatever reason" },
    { id: 5, name: "filling out reports after reports" },
    { id: 6, name: "شيء مهم" },
    { id: 7, name: "yet another super long named tag for whatever reason" },
    { id: 8, name: "watching TV" },
    { id: 9, name: "Что-то очень важное" },
    { id: 10, name: "重要的事情" },
    { id: 11, name: "procrastination" },
];

const dummyFromDate = new Date();
dummyFromDate.setDate(dummyFromDate.getDate() - 5);
const dummyToDate = Date.now();

const dummyTag = {
    id: 1,
    name: "some tag",
};

const dummyChartData = [
    {
        title: "One",
        value: 10,
        color: "blue",
    },
    {
        title: "Two",
        value: 15,
        color: "orange",
    },
    {
        title: "Three",
        value: 20,
        color: "red",
    },
];

storiesOf("Tag Picker", module)
    .add("Default", () => <TagPicker></TagPicker>)
    .add("With Tags", () => (
        <TagPicker
            tags={dummyTags}
            onDelete={action("onDelete clicked!")}
        ></TagPicker>
    ));

storiesOf("Tag", module)
    .add("Default", () => <Tag></Tag>)
    .add("With Props", () => (
        <Tag
            id={dummyTag.id.toString()}
            name={dummyTag.name}
            onToggle={action("onToggle clicked!")}
        ></Tag>
    ));

storiesOf("Date Selector", module)
    .add("Default", () => (
        <DateSelector onUpdate={action("dates updated!")}></DateSelector>
    ))
    .add("With date pre-filled", () => (
        <DateSelector
            onUpdate={action("dates updated!")}
            from={dummyFromDate}
            to={dummyToDate}
        ></DateSelector>
    ))
    .add("Buttons disabled", () => (
        <DateSelector
            active={false}
            from={dummyFromDate}
            to={dummyToDate}
        ></DateSelector>
    ));

storiesOf("Chart", module)
    .add("Default", () => <Chart></Chart>)
    .add("With data", () => <Chart data={dummyChartData}></Chart>);
